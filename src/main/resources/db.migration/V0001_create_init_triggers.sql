ALTER TABLE customers
ADD COLUMN last_updated timestamp;

CREATE OR REPLACE FUNCTION track_changes_on_customers()
RETURNS trigger AS $$
BEGIN
        SET TIME ZONE 'Asia/Almaty';
        NEW.last_updated = now();
        RETURN NEW;
END
$$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS customers_timestamp ON customers;
CREATE TRIGGER customers_timestamp BEFORE INSERT OR UPDATE ON customers
    FOR EACH ROW EXECUTE PROCEDURE track_changes_on_customers()