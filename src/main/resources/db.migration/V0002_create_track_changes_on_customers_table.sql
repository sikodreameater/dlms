CREATE TABLE IF NOT EXISTS track_changes_on_customers
(
    id            BIGSERIAL PRIMARY KEY,
    time          TIMESTAMP,
    user_changed  VARCHAR(64),
    attempt_count BIGINT DEFAULT(1)
)
